

import midi
import random
#from markov import MarkovBuilder

pattern = midi.read_midifile('Mind_Heist.mid')

max_tick = 500



chain = []			#chain of events in music
last = None			#last event item that occured

tempo_inst_num = 0
tempo_event = None
microseconds_per_quarter_note = None

for i in list(range(len(pattern))):		#for each track
	for event in pattern[i]:		#for each event in a track
		paste = event
		
		if isinstance(event, midi.events.NoteOnEvent):

			contains = False			
			for group in chain:			#for each grouping of events in the chain

				if last == group[0]:		#if the current event is the last one that occured
					group[1].append(event)
				if event == group[0]:		#check if this event has ever occured before
					contains = True
			if not contains:			#if it wasn't in the chain, add to chain
				chain.append([event, []])
			last = event
		elif isinstance(event, midi.events.SetTempoEvent) and tempo_inst_num == 0:
			tempo_event = event
			tempo_inst_num+=1


# Instantiate a MIDI Pattern (contains a list of tracks)
pattern = midi.Pattern()
# Instantiate a MIDI Track (contains a list of MIDI events)
track = midi.Track()
# Append the track to the pattern
pattern.append(track)


if tempo_event is not None:
	track.append(tempo_event)


#random.choice()
#random.seed(0)
length = 1000

index = 0
event = chain[index][0]

for i in list(range(length)):
	track.append(event)
	if len(chain[index][1]) != 0:
		event = random.choice(chain[index][1])
		for ind, group in enumerate(chain):
			if event == group[0]:
				index = ind
	else:
		break
		
# Add the end of track event, append it to the track
eot = midi.EndOfTrackEvent(tick=1)
track.append(eot)


#clean up section
#print('\n\nclean up section:\n')
#print(pattern[0][0])
for event in pattern[0]:
	#print(type(event))
	if type(event) == type(midi.events.NoteOnEvent()):
		#print(event.tick)
		
		if event.tick > max_tick:
			event.tick = max_tick
			
	#event.tick *= 3



f = open('output.txt', 'w')
	
# Print out the pattern
f.write(str(pattern))
# Save the pattern to disk
midi.write_midifile(str('output.mid'), pattern)


pattern = midi.read_midifile('output.mid')
f.write(str(pattern))
f.close()