import midi
import random
from markov import MarkovBuilder
from itertools import chain, ifilter
from collections import defaultdict
import sys

f = open('output.txt', 'w')

midi.NoteOnEvent.__hash__ = lambda x: hash(repr(x))

input = midi.read_midifile('strauss_2049s_blue_danube_waltz_(nc)smythe.mid')

max_tick = 500
length = 1000
markov_order = 1

# Instantiate a MIDI Pattern (contains a list of tracks)
pattern = midi.Pattern()

#channels = defaultdict(list)

#tempo = next(ifilter(lambda e: isinstance(e, midi.events.SetTempoEvent), chain(*input)))

events = list(ifilter(lambda e: isinstance(e, midi.events.NoteOnEvent), chain(*input)))

#print(events)

markov = MarkovBuilder(order=markov_order)
markov.train(events)

model = markov.build()

track = midi.Track()
pattern.append(track)





for event in model.generate(length):
	#f.write(str(event))
	if event.tick > max_tick:
		event.tick = max_tick

	track.append(event)

track.append(midi.EndOfTrackEvent(tick=1))

midi.write_midifile("output.mid", pattern)


pattern = midi.read_midifile("output.mid")
f.write(str(pattern))
f.close()

"""
for event in ifilter(lambda e: isinstance(e, midi.events.NoteOnEvent), chain(*input)):
	channels[event.channel].append(event)

class Note(object):
	def __init__(self, pitch, velocity, duration):
		self.pitch = pitch
		self.velocity = velocity
		self.duration = duration
	
	def start_event(self, channel):
		return midi.events.NoteOnEvent(tick=0, channel=channel,
									   data=[self.pitch, self.velocity])
	
	def end_event(self, channel):
		return midi.events.NoteOnEvent(tick=0, channel=channel,
									   data=[self.pitch, 0])
	
	def __repr__(self):
		return 'Note(pitch=%d, velocity=%d, duration=%d)' % (self.pitch, self.velocity, self.duration) 
	
	def __hash__(self):
		return hash((0, self.pitch, self.velocity, self.duration))
		
class Rest(object):
	def __init__(self, duration):
		self.duration = duration

	def __repr__(self):
		return 'Rest(duration=%d)' % (self.duration,)
		
	def __hash__(self):
		return hash((1, self.duration))
		
def group_notes(events):
	ticks_since_last_note_start = 0

	for i, start in enumerate(events):
		pitch, velocity = start.data
		ticks_since_last_note_start += start.tick
		
		if velocity == 0:
			continue
		
		duration = 0
		try:
			for end in events[i+1:]:
				duration += end.tick
				end_pitch, end_velocity = end.data
				if pitch == end_pitch and end_velocity == 0:
					if ticks_since_last_note_start > 0:
						yield Rest(ticks_since_last_note_start)
					yield Note(pitch, velocity, duration)
					ticks_since_last_note_start = 0
					break
		except IndexError:
			pass

def event_time_pairs(notes, channel):
	ticks = 0
	for note in notes:
		if isinstance(note, Note):
			yield (ticks, note.start_event(channel))
			yield (ticks + note.duration, note.end_event(channel))
		elif isinstance(note, Rest):
			ticks += note.duration
		else:
			raise 'Not Note or Rest'

def notes_to_events(notes, channel):
	last_tick = 0
	for tick, event in sorted(event_time_pairs(notes, channel), key=lambda x: x[0]):
		event.tick = tick - last_tick
		yield event
		last_tick = tick

channel_notes = {}


with open('notes.txt', 'w') as f:
	for channel, events in channels.iteritems():
		f.write('\n\n\nCHANNEL\n')
		f.write('\n\n\n\nEVENTS:\n')
		f.write('\n'.join(map(str, events)))
		channel_notes[channel] = list(group_notes(events))
		f.write('\n\n\n\nNOTES:\n')
		f.write('\n'.join(map(str, channel_notes[channel])))
	
for channel, notes in channel_notes.iteritems():
	notes = list(notes)
	
	if len(notes) == 0:
		continue
		
	with open('prayer.txt', 'w') as f:
		f.write('\n\nNOTES:\n')
		f.write('\n'.join(map(str, notes)))
		f.write('\n\nEVENTS:\n')
		f.write('\n'.join(map(str, notes_to_events(notes, channel))))

	markov = MarkovBuilder(order=markov_order)
	markov.train(notes)
	gen = markov.build()
	track = midi.Track()
	pattern.append(track)
	
	#track.append(tempo)

	with open('stuff.txt', 'w') as f:
		output_notes = list(gen.generate(length))
		f.write('\n'.join(map(str, output_notes)) + '\n\n\n')
		for event in notes_to_events(output_notes, channel):
			f.write(str(event) + '\n')
			track.append(event)
		
	track.append(midi.EndOfTrackEvent(tick=1))
"""
	
	
# #clean up section
# #print('\n\nclean up section:\n')
# #print(pattern[0][0])
# for event in pattern[0]:
	# #print(type(event))
	# if type(event) == type(midi.events.NoteOnEvent()):
		# #print(event.tick)
		
		# if event.tick > max_tick:
			# event.tick = max_tick
			
	# event.tick *= 3



# Print out the pattern
#print(pattern)
# Save the pattern to disk
