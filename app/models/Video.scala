package models

import java.net.URL

case class Slide(image: URL, paddingBeforeMilliseconds: Int, paddingAfterMilliseconds: Int, text: String)

case class Video(slides: Seq[Slide])
