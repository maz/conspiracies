package models

import services.Entity

case class Request(startingNode: Entity.Id, numberOfSteps: Int)
