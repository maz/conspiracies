package models

import play.api.libs.json.Json

case class RenderedSlide(startTime: Int, endTime: Int, text: String, image: String)

case class RenderedVideo(images: Map[String, RenderedVideo.DataURL], combinedAudio: RenderedVideo.DataURL,
                         slides: Seq[RenderedSlide])

object RenderedVideo {
  type DataURL = String

  implicit val fmt1 = Json.format[RenderedSlide]
  implicit val fmt2 = Json.format[RenderedVideo]
}
