package services

import java.util.concurrent.ThreadLocalRandom
import javax.inject.{Inject, Singleton}

import models.{Request, Slide, Video}
import services.Walker.Entry

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Random

trait Walker {
  def walkRequest(req: Request): Future[Seq[Walker.Entry]]
  def wordify(entries: Seq[Entry]): Future[Video]
}

object Walker {

  case class Entry(name: String, description: String, `type`: String, relationshipToNext: Option[String],
                   value: Option[String])

}

@Singleton
class WalkerImpl @Inject()(wd: WikiData, im: ImageSearch) extends Walker {
  def wordify(entries: Seq[Entry]): Future[Video] = {
    val out = Wordifier.createString(entries.map {
      case Entry(a,b,c,d,e) => Array[String](a,b,c,d.orNull, e.orNull)
    }.toArray)
    out match {
      case Array(txts, terms) => Future.sequence(txts.toSeq.zip(terms.toSeq).map {
        case (txt, term) => im.search(term).map(url => Slide(url, 2, 2, txt))
      }).map(Video)
    }
  }

  override def walkRequest(req: Request): Future[Seq[Entry]] = {
    def step(remainingSteps: Int, entity: Entity): Future[Seq[Entry]] = {
      sealed trait PathStep {
        def dest: Entity.Id
      }
      case class NumberOne(property: Property.Id, dest: Entity.Id) extends PathStep
      case class NumberTwo(property: Property.Id, entity: Entity.Id, dest: Entity.Id) extends PathStep

      def step2(steps: Seq[PathStep], entity: Entity, i: String): Future[Seq[Entry]] = {
        if (steps.size == 0)
          Future.successful(Seq(Entry(entity.name, entity.description, i, None, None)))
        else {
          val partial = for {
            dest <- wd.entity(steps.head.dest)
            rest <- step(remainingSteps - 1, dest)
            // Seq(Entry(entity.name, entity.description, i, None, None))
            particular <- steps.head match {
              case NumberOne(p, _) => wd.propertyName(p).map(
                pn => Seq(Entry(entity.name, entity.description, i, Some(pn), None)))
              case NumberTwo(p, v, _) => for {
                pn <- wd.propertyName(p)
                vn <- wd.entity(v).map(_.name)
              } yield Seq(Entry(entity.name, entity.description, i, Some(pn), Some(vn)))
            }
            candidate = particular ++ rest
          } yield candidate
          partial.flatMap {
            case x if x.size >= remainingSteps => Future.successful(x)
            case x => step2(steps.tail, entity, i)
          }
        }
      }

      val instanceof = entity.claims.view.find(_._1 == "P31").map(_._2).map(wd.entity).map(
        _.map(_.name)).getOrElse(Future.successful(""))
      if (remainingSteps == 0)
        instanceof.map(i => Seq(Entry(entity.name, entity.description, i, None, None)))
      else {
        for {
          i <- instanceof
          numberTwos <- Future.sequence(
            (1 to 4).map((_: Int) => entity.claims(ThreadLocalRandom.current().nextInt(entity.claims.size))).toSet.map(
              (pair: (Property.Id, Entity.Id)) => pair match {
                case (a: Property.Id, b: Entity.Id) => wd.findEntitiesWithPropertyAndValue(a, b).filter(_.size > 0).map(
                  x => (NumberTwo(a, b, x(ThreadLocalRandom.current().nextInt(x.size)))))
              })).map(_.toSet)
          numberOnes: Set[PathStep] = (1 to 4).map(
            _ => entity.claims(ThreadLocalRandom.current().nextInt(entity.claims.size))).toSet
            .map((pair: (Property.Id, Entity.Id)) => pair match {
              case (a, b) => NumberOne(a, b)
            })
          combined: Set[PathStep] = new Random(ThreadLocalRandom.current()).shuffle(
            numberOnes.toSet ++ numberTwos.toSet)
          result <- step2(combined.toSeq, entity, i)
        } yield result
      }
    }
    wd.entity(req.startingNode).flatMap(x => step(req.numberOfSteps - 1, x))
  }
}
