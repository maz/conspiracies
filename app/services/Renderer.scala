package services

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, File, FileInputStream}
import java.net.URL
import java.nio.file.Files
import java.util
import javax.inject.{Inject, Singleton}
import javax.sound.sampled.{AudioFileFormat, AudioFormat, AudioInputStream, AudioSystem}
import javax.xml.bind.DatatypeConverter

import marytts.util.data.BufferedDoubleDataSource
import marytts.util.data.audio.{DDSAudioInputStream, SequenceAudioInputStream}
import models.{RenderedSlide, RenderedVideo, Slide, Video}
import org.apache.commons.codec.binary.{Base64, StringUtils}
import org.apache.commons.io.IOUtils
import play.api.cache.CacheApi
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

trait Renderer {
  def render(video: Video): Future[RenderedVideo]
}

object TTS {
  def apply(msg: String): AudioInputStream = {
    /*val out = new ByteArrayOutputStream()
    Mary.process(msg, "TEXT", "AUDIO", "en_GB", "", "", "", "", "WAVE_FILE", out)
    return AudioSystem.getAudioInputStream(new ByteArrayInputStream(out.toByteArray))*/
    /*val mary = new LocalMaryInterface
    mary.setLocale(Locale.UK)
    mary.setVoice(Voice.getAvailableVoices.iterator().next().getName)
    mary.generateAudio(msg)*/
    val dst = File.createTempFile("dank", ".wav")
    try {
      Runtime.getRuntime.exec(Array[String]("/usr/bin/env", "espeak", "-s", "120", msg, "-w", dst.toString)).waitFor()
      val fis = new FileInputStream(dst)
      val bytes = IOUtils.toByteArray(fis)
      fis.close()
      AudioSystem.getAudioInputStream(new ByteArrayInputStream(bytes))
    } finally {
      Try(Files.delete(dst.toPath))
    }
  }
}

@Singleton
class RendererImpl @Inject()(cache: CacheApi, ws: WSClient) extends Renderer {
  private def silence(milliseconds: Int, format: AudioFormat): AudioInputStream = {
    new DDSAudioInputStream(new BufferedDoubleDataSource(
      new Array[Double](((milliseconds.toDouble / 1000) * format.getFrameRate.toDouble).toInt)), format)
  }

  private def magic(x: Seq[(Slide, Int)]): Seq[RenderedSlide] = {
    val out = Seq.newBuilder[RenderedSlide]
    var time = 0
    x.foreach {
      case (s, delta) => {
        out += RenderedSlide(time, time + delta, s.text, s.image.toString)
        time += delta
      }
    }
    out.result()
  }

  override def render(video: Video): Future[RenderedVideo] = for {
    images <- Future.sequence(video.slides.map(_.image).toSet[URL].map((url: URL) => cache.getOrElse(url.toString) {
      for {
        resp <- ws.url(url.toString).withHeaders("Accept" -> "image/*").get()
      } yield (url.toString, s"data:${
        resp.header("Content-Type").getOrElse("image/jpeg")
      };base64," + StringUtils.newStringUtf8(Base64.encodeBase64(resp.bodyAsBytes.toArray, false)).replaceAll("\\-","+").replaceAll("_","/"))
    })).map(_.toMap)
    ttsStrings: Map[String, AudioInputStream] = video.slides.map(_.text).toSet.map((x: String) => (x, TTS(x))).toMap
    format = ttsStrings.head._2.getFormat
    audios: Seq[AudioInputStream] = video.slides.flatMap(
      slide => Seq(silence(slide.paddingBeforeMilliseconds, format), ttsStrings(slide.text),
        silence(slide.paddingAfterMilliseconds, format)))
    combinedAudio = new SequenceAudioInputStream(format, util.Arrays.asList(audios.toArray: _*))
    bao = new ByteArrayOutputStream()
    _ = AudioSystem.write(combinedAudio, AudioFileFormat.Type.WAVE, bao)
  } yield RenderedVideo(images, "data:audio/wav;base64," + StringUtils.newStringUtf8(Base64.encodeBase64(bao.toByteArray, false)).replaceAll("\\-","+").replaceAll("_","/"),
    magic(video.slides.map(s => (s, s.paddingAfterMilliseconds + s.paddingBeforeMilliseconds + (ttsStrings(
      s.text).getFrameLength.toDouble / format.getFrameRate.toDouble).toInt)))
  )
}
