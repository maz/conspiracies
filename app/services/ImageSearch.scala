package services

import java.net.{URL, URLEncoder}
import javax.inject.{Inject, Singleton}

import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait ImageSearch {
  def search(term: String): Future[URL]
}

@Singleton
class ImageSearchImpl @Inject()(ws: WSClient) extends ImageSearch {
  override def search(term: String): Future[URL] =
    ws.url(s"https://api.cognitive.microsoft.com/bing/v5.0/images/search?q=${
      URLEncoder.encode(term, "UTF-8")
    }&count=1&safeSearch=Strict")
      .withHeaders("Ocp-Apim-Subscription-Key" -> "51b3405b347540d79edf339bebc897cb")
      .get().map(
      resp => Json.parse(resp.body).as[Map[String, JsValue]].apply("value").as[Seq[Map[String, JsValue]]].head.apply(
        "contentUrl").as[String]).map((x: String) => new URL(x))
}

