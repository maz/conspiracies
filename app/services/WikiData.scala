package services

import java.net.URLEncoder
import javax.inject.{Inject, Singleton}

import play.api.cache.CacheApi
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient
import services.Property.Id
import services.WikiData.SearchResult

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Success, Try}

object Property {
  type Id = String
}

case class Entity(name: String, description: String, claims: Seq[(Property.Id, Entity.Id)])

object Entity {
  type Id = String
}

trait WikiData {
  def propertyName(id: Property.Id): Future[String]

  def entity(id: Entity.Id): Future[Entity]

  def findEntitiesByName(name: String): Future[Seq[WikiData.SearchResult]]

  def findEntitiesWithPropertyAndValue(prop: Property.Id, value: Entity.Id): Future[Seq[Entity.Id]]
}

object WikiData {

  case class SearchResult(id: Entity.Id, name: String, description: String)

}

@Singleton
class WikiDataImpl @Inject()(cache: CacheApi, ws: WSClient) extends WikiData {

  override def propertyName(id: Property.Id): Future[String] = getJson(
    s"https://www.wikidata.org/w/api.php?action=query&titles=${
      URLEncoder.encode("Property:" + id, "utf-8")
    }&prop=revisions&rvprop=content&format=json")
    .map(_.apply("query").as[Map[String, JsValue]].apply(
      "pages").as[Map[String, Map[String, JsValue]]].values.head.apply(
      "revisions").as[Seq[Map[String, String]]]).map(_.head("*"))
    .map { rawJson =>
      val j = Json.parse(rawJson).as[Map[String, JsValue]]
      j("labels").as[Map[String, Map[String, String]]].apply("en")("value")
    }

  override def entity(id: Entity.Id): Future[Entity] =
    getJson(s"https://www.wikidata.org/w/api.php?action=query&titles=${
      URLEncoder.encode(id, "utf-8")
    }&prop=revisions&rvprop=content&format=json")
      .map(_.apply("query").as[Map[String, JsValue]].apply(
        "pages").as[Map[String, Map[String, JsValue]]].values.head.apply(
        "revisions").as[Seq[Map[String, String]]]).map(_.head("*"))
      .map { rawJson =>
        val j = Json.parse(rawJson).as[Map[String, JsValue]]
        Entity(j("labels").as[Map[String, Map[String, String]]].apply("en")("value"),
          j("descriptions").as[Map[String, Map[String, String]]].get("en").map(_.apply("value")).getOrElse(""),
          j("claims").as[Map[String, JsValue]].toSeq.map {
            case (propId, propValue) => Try {
              propValue.as[Seq[Map[String, JsValue]]].map({ p =>
                p.apply("mainsnak").as[Map[String, JsValue]].get("datavalue").flatMap(_.as[Map[String, JsValue]].apply(
                  "value").asOpt[Map[String, JsValue]])
              }).collect { case Some(x) => x }
                .filter(_.get("entity-type").flatMap(_.asOpt[String]).contains("item"))
                .map(x => (propId, x.apply("id").as[String]))
            }
          }.collect { case Success(x) => x }.flatten)
      }


  override def findEntitiesByName(name: String): Future[Seq[WikiData.SearchResult]] =
    getJson(s"https://www.wikidata.org/w/api.php?action=wbsearchentities&search=${
      URLEncoder.encode(name, "UTF-8")
    }&format=json&language=en&uselang=en&type=item").map(_.apply("search").as[Seq[Map[String, JsValue]]]
      .filter(x => Seq("id", "label", "description").forall(y => x.keySet.contains(y)))
      .map(m => SearchResult(m("id").as[String], m("label").as[String], m("description").as[String]))
    )

  override def findEntitiesWithPropertyAndValue(prop: Id,
                                                value: Id): Future[Seq[Id]] = {
    val query =
      s"""
         |SELECT ?s ?desc WHERE {
         |  ?s wdt:$prop wd:$value .
         |  OPTIONAL {
         |     ?s rdfs:label ?desc filter (lang(?desc) = "en").
         |   }
         | }
       """.stripMargin
    getJson(
      s"https://query.wikidata.org/bigdata/namespace/wdq/sparql?query=${URLEncoder.encode(query, "UTF-8")}&format=json")
      .map(_.apply("results").as[Map[String, JsValue]].apply("bindings").as[Seq[Map[String, JsValue]]].map(
        _.apply("s").as[Map[String, String]].apply("value").split("/").last))
  }

  private def getJson(url: String, accepts: Option[String] = None): Future[Map[String, JsValue]] = cache.getOrElse(url)
  {
    ws.url(url).get().map(r => Json.parse(r.body)).map(_.as[Map[String, JsValue]])
  }
}
