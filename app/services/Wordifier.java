package services;

public class Wordifier {

    private static String sameType(String first, String second, String rel, String val) {
        String str;
        if (rel.startsWith("member") || rel.startsWith("part")) {
            str = first + " and " + second + " both are a " + rel + " " + val;
        } else {
            str = first + " and " + second + " both have " + rel + " " + val;
        }
        return str;
    }

    private static String diffTypes(String str, String type1, String type2, String relation, String next) {
        if (type1.equals("business enterprise") && (type2.equals("city") || type2.equals("country"))) {
            str += "is in " + next;
        } else if (type1.equals("human") && (type2.equals("city") || type2.equals("country"))) {
            str += "is from " + next;
        } else if ((type1.equals("city") || type1.equals("country")) && type2.equals("human")) {
            str += "is home to " + next;
        } else if (relation.endsWith("es") || relation.endsWith("ed")) {
            str += relation + " " + next;
        } else if (relation.endsWith("s")) {
            str += "is " + relation + " " + next;
        } else {
            str += "has " + relation + " " + next;
        }
        return str;
    }

    public static String[][] createString(String[][] arr) {

        String[] readInput = new String[arr.length];
        String[] searchTerms = new String[arr.length];

        for (int i = 0; i < arr.length; i++) {
            // creates a new string based on 
            if (i < arr.length - 1) {
                if (arr[i][4] != null) {
                    readInput[i] = sameType(arr[i][0], arr[i + 1][0], arr[i][3], arr[i][4]);
                } else {
                    readInput[i] = arr[i][0] + " ";
                    readInput[i] = diffTypes(readInput[i], arr[i][2], arr[i + 1][2], arr[i][3], arr[i + 1][0]);
                }
            }
            searchTerms[i] = arr[i][0] + " " + arr[i][1];
        }
        readInput[arr.length - 1] = arr[0][0] + " did " + arr[arr.length - 1][0];
        String[][] strings = {readInput, searchTerms};
        return strings;
    }

    /*public static void main(String[] args) {
//        String[][] arr = {{"Damn Daniel", "2016 Viral Video", "meme", "uses", null}, {"Vans", "Shoe Company", "business enterprise", "headquarters location", null}, {"Cypress, Los Angeles, CA", "city", "city", "elevation", null}, {"12.3 m", "height in meters", "measurement", "sum of first two terms", null}, {"3", "number", "digit", "sides of", null}, {"triangle", "shape", "shape", "symbol of",null}, {"Illuminati","secret society", "secret society", "killed", null},{"John F. Kennedy","president","human","cause of death", "ballistic trauma"}, {"Harambe", "gorilla","animal",null,null}};

        String[][] = {{"John F. Kennedy, American politician", "35th president of the United States", "human", "religion", "Catholicism"}, {"Mary of Burgundy, Duchess of Burgundy", 

        String[][] test = createString(arr);
        
        for (int j = 0; j < test[0].length; j++) {
            if (test[0][j] != null) {
                System.out.printf("%s\n", test[0][j]);
            }
        }

        
    }*/
}
