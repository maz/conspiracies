package controllers

import javax.inject._

import akka.actor.ActorSystem
import play.api.mvc._
import services._
import scala.util._

import scala.concurrent._

/**
  * This controller creates an `Action` that demonstrates how to write
  * simple asynchronous code in a controller. It uses a timer to
  * asynchronously delay sending a response for 1 second.
  *
  * @param actorSystem We need the `ActorSystem`'s `Scheduler` to
  *                    run code after a delay.
  * @param exec        We need an `ExecutionContext` to execute our
  *                    asynchronous code.
  */
@Singleton
class AsyncController @Inject()(actorSystem: ActorSystem, walker: Walker, renderer: Renderer)
                               (implicit exec: ExecutionContext) extends Controller {

  import models.RenderedVideo._
  val things = new java.util.concurrent.ConcurrentHashMap[Int, models.RenderedVideo]()

  /**
    * Create an Action that returns a plain text message after a delay
    * of 1 second.
    *
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/message`.
    */
  private def thing(id: String) = 
  walker.walkRequest(models.Request(id, 4))
        .flatMap(walker.wordify)
        .flatMap(renderer.render)
        .map(x => {
            val id = java.util.concurrent.ThreadLocalRandom.current().nextInt()
            things.put(id, x)
            Redirect(s"/assets/c.html?$id")
        })
  def generate: play.api.mvc.Action[play.api.mvc.AnyContent] = Action.async { (req: play.api.mvc.Request[AnyContent]) =>
      var i = 5
      var out: Future[play.api.mvc.Result] = null
      while(i >= 0) {
          try {
              out = thing(req.queryString("id").head)
              i = -666
          } catch {
              case _: Throwable => ()
          }
          i -= 1
      }
      out
  }
  def get(x: Int) = Action {
      Ok(models.RenderedVideo.fmt2.writes(things.get(x)))
  }

}
