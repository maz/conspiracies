name := "play-scala"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

resolvers += "Mary" at "https://jcenter.bintray.com/"

libraryDependencies ++= Seq(
  "de.dfki.mary" % "voice-dfki-obadiah-hsmm" % "5.2",
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test
)

